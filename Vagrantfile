servers = [
    {
        :hostname => "ansible-serveur",
        :ip => "192.168.56.10",
        :ram => 2048,
        :cpu => 1,
        :vram => 128,
        :type => "client"
    },
    {
        :hostname => "ansible-client",
        :ip => "192.168.56.11",
        :ram => 512,
        :cpu => 1,
        :vram => 8,
        :type => "serveur"
    }
]

Vagrant.configure(2) do |config|
    # Paramétrage des VM
    config.vm.box_check_update = true
    servers.each do |server|
        config.vm.define server[:hostname] do |node|
            node.vm.box = "generic/debian10"
            
            node.vm.synced_folder ".", "/vagrant", type: "rsync"
            node.vm.hostname = server[:hostname]
            node.vm.network "private_network", ip: server[:ip]
  
            node.vm.provider "virtualbox" do |v|
                v.gui = false
                v.name = server[:hostname]
                v.memory = server[:ram]
                v.cpus = server[:cpu]
                v.customize ["modifyvm", :id, "--vram", server[:vram]]
            end

            #Initiatlisation des VM
            node.vm.provision :shell, inline: <<-SHELL
                # Update et installation des paquets
                echo "Update et installation des paquets"
                apt-get update
                apt-get full-upgrade -y
                DEBIAN_FRONTEND=noninteractive apt-get install -y vim task-french keyboard-configuration console-setup htop curl tree dos2unix

                # Paramétrage du français
                echo "Paramétrage du français"
                echo -e "LANG=fr_FR.UTF-8\nLC_ALL=fr_FR.UTF-8" > /etc/default/locale
                echo "fr_FR.UTF-8 UTF-8" > /etc/locale.gen
                locale-gen
                echo -e 'XKBMODEL="pc105"\nXKBLAYOUT="fr"\nXKBVARIANT="azerty"\nXKBOPTIONS=""\nBACKSPACE="guess"' > /etc/default/keyboard
                sed -i 's/KEYMAP=n/KEYMAP=y/' /etc/initramfs-tools/initramfs.conf

                # Disable mouse in vim
                echo "Disable mouse in vim"
                echo -e "set mouse=\nset ttymouse=" > /etc/vimrc
                echo -e "set mouse=\nset ttymouse=" > /home/vagrant/.vimrc

                # Set vagrant password
                echo "Set vagrant password"
                usermod --password $(echo vagrant | openssl passwd -1 -stdin) vagrant
                SHELL

            node.vm.provision :reload

            if server[:hostname] == "ansible-serveur"
                node.vm.provision :shell, inline: <<-SHELL
                    ## Installation des paquets nécessaires au fonctionnement de le VM
                    echo "Installation des paquets nécessaires au fonctionnement de le VM"
                    apt-get install -y xfce4 xfce4-terminal gnupg2 software-properties-common apt-transport-https linux-headers-$(uname -r) gcc make perl firefox-esr

                    ## Installation des virtualbox guest
                    cd /vagrant/VM_package
                    tar -xzf VBox_GAs_6.1.32.tar.gz
                    cd VBox_GAs_6.1.32/
                    ./VBoxLinuxAdditions.run
                    
                    ## Installation d'Ansible
                    echo "Installation d'Ansible"
                    echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu bionic main" > /etc/apt/sources.list.d/ansible.list
                    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
                    apt update
                    apt install -y ansible
                    
                    ## Auto-login au démarrage du serveur Ansible
                    echo "Auto-login au démarrage du serveur Ansible"
                    echo -e "[Seat:*]\nautologin-user=vagrant\nautologin-user-timeout=0" >> /etc/lightdm/lightdm.conf                

                    ## Paramétrage d'Ansible
                    echo "Paramétrage d'Ansible"
                    echo "192.168.56.11    ansible-client" >> /etc/hosts
                    echo -e "all:\n  children:\n    atelier:\n      hosts:\n        ansible-serveur:\n          ansible_host: localhost\n          ansible_connection: local\n        ansible-client:\n          ansible_host: 192.168.56.11\n          ansible_ssh_user: vagrant\n          ansible_ssh_private_key_file: /home/vagrant/.ssh/ansible\n          ansible_ssh_common_args: "-o StrictHostKeyChecking=no"" > /etc/ansible/hosts
                    chown vagrant: -R /home/ansible
                    chown vagrant: -R /etc/ansible

                    # Récupération des clefs SSH
                    echo "Récupération des clefs SSH"
                    cp /vagrant/VM_package/ansible /home/vagrant/.ssh/ansible
                    cp /vagrant/VM_package/ansible.pub /home/vagrant/.ssh/ansible.pub
                    echo -e "Host ansible-client\n  IdentityFile ~/.ssh/ansible" >> /home/vagrant/.ssh/config
                    dos2unix /home/vagrant/.ssh/*
                    chown -R vagrant: /home/vagrant/.ssh
                    chmod 0700 -R /home/vagrant/.ssh
                    chmod 0644 /home/vagrant/.ssh/ansible.pub
                    chmod 0644 /home/vagrant/.ssh/authorized_keys
                    chmod 0644 /home/vagrant/.ssh/config
                    chmod 0600 /home/vagrant/.ssh/ansible
                    chmod 0755 /home/vagrant
                SHELL
                node.vm.provision :reload
                node.vm.provision :shell, inline: <<-SHELL
                    ## Installation de Vscode pour travailler sur le serveur Ansible
                    echo "Installation de Vscode pour travailler sur le serveur Ansible"
                    curl -sSL https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
                    add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
                    apt-get update
                    apt-get install -y code
                    code --install-extension redhat.ansible --user-data-dir vagrant
                    
                    ## Lancement de Vscode à l'ouverture de session
                    mkdir -p /home/vagrant/.config/autostart
                    echo -e '[Desktop Entry]\nEncoding=UTF-8\nVersion=0.9.4\nType=Application\nName=codium\nComment=\nExec=code /etc/ansible\nOnlyShowIn=XFCE;\nStartupNotify=false\nTerminal=false\nHidden=false\n' > /home/vagrant/.config/autostart/vscode_startup.desktop
                    echo -e '[Desktop Entry]\nEncoding=UTF-8\nVersion=0.9.4\nType=Application\nName=installation codium\nComment=\nExec=code --install-extension redhat.ansible --user-data-dir vagrant\nOnlyShowIn=XFCE;\nStartupNotify=false\nTerminal=false\nHidden=false\n' > /home/vagrant/.config/autostart/vscode_settings.desktop

                    if [[ -d /home/vagrant/Desktop ]]; then echo -e '{\n  "folders": [\n    {\n      "path": "/etc/ansible"\n    }\n  ],\n  "settings": {}\n}' >> /home/vagrant/Desktop/ansible.code-workspace; fi
                    if [[ -d /home/vagrant/Bureau ]]; then echo -e '{\n  "folders": [\n    {\n      "path": "/etc/ansible"\n    }\n  ],\n  "settings": {}\n}' >> /home/vagrant/Bureau/ansible.code-workspace; fi
                    chown -R vagrant:vagrant /home/vagrant
                SHELL
            node.vm.provision :reload
            end
            if server[:hostname] == "ansible-client"
                node.vm.provision :shell, inline: <<-SHELL
                    # Récupération des clefs SSH
                    echo "Récupération des clefs SSH"
                    cat /vagrant/VM_package/ansible.pub >> /home/vagrant/.ssh/authorized_keys
                    dos2unix /home/vagrant/.ssh/*
                    chown -R vagrant: /home/vagrant/.ssh
                    chmod 0700 /home/vagrant/.ssh
                    chmod 0644 /home/vagrant/.ssh/authorized_keys
                    chmod 0755 /home/vagrant
                SHELL
            end
        end
    end
end
